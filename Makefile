# Date : 21/10/2021

DC=docker-compose
DCD=docker-compose -f docker-compose-dev.yml
DCP=docker-compose -f docker-compose-prod.yml
DCT=docker-compose -f docker-compose-test.yml

.DEFAULT_GOAL := help

.PHONY: help ## Generate list of targets with descriptions
help:
		@grep '##' Makefile \
		| grep -v 'grep\|sed' \
		| sed 's/^\.PHONY: \(.*\) ##[\s|\S]*\(.*\)/\1:\2/' \
		| sed 's/\(^##\)//' \
		| sed 's/\(##\)/\t/' \
		| expand -t14

## Project setup & day to day shortcuts

.PHONY: local
local:
	$(DC) pull || true
	$(DC) build
	$(DC) up -d
	$(DC) exec backend composer install
	sleep 5
	$(DC) exec backend php bin/console doctrine:database:create --if-not-exists
	$(DC) exec backend php bin/console doctrine:schema:update --force
	$(DC) exec backend php bin/console hautelook:fixtures:load -q

.PHONY: dev
dev:
	$(DCD) pull || true
	$(DCD) -p athena_dev up -d

.PHONY: prod
prod:
	$(DCP) pull || true
	$(DCP) -p athena_prod up -d

.PHONY: test
test:
	$(DCT) pull || true
	$(DCT) -p athena_test up -d

.PHONY: all
all:
	make local
	make dev
	make prod
	make test

#####################
# DEV DOCKER IMAGES #
#####################

.PHONY: build-frontend-dev
build-frontend-dev:
	docker build -t registry.gitlab.com/lorddistrict/athena:athena-frontend-dev . -f docker/frontend/dev/Dockerfile
	docker push registry.gitlab.com/lorddistrict/athena:athena-frontend-dev

.PHONY: build-backend-dev
build-backend-dev:
	docker build -t registry.gitlab.com/lorddistrict/athena:athena-backend-dev . -f docker/backend/dev/Dockerfile
	docker push registry.gitlab.com/lorddistrict/athena:athena-backend-dev

########################################################################################################################

.PHONY: build-frontend-prod
build-frontend-prod:
	docker build -t registry.gitlab.com/lorddistrict/athena:athena-frontend-prod . -f docker/frontend/prod/Dockerfile
	docker push registry.gitlab.com/lorddistrict/athena:athena-frontend-prod

.PHONY: build-backend-prod
build-backend-prod:
	docker build -t registry.gitlab.com/lorddistrict/athena:athena-backend-prod . -f docker/backend/prod/Dockerfile
	docker push registry.gitlab.com/lorddistrict/athena:athena-backend-prod

########################################################################################################################

.PHONY: build-frontend-test
build-frontend-test:
	docker build -t registry.gitlab.com/lorddistrict/athena:athena-frontend-test . -f docker/frontend/test/Dockerfile
	docker push registry.gitlab.com/lorddistrict/athena:athena-frontend-test

.PHONY: build-backend-test
build-backend-test:
	docker build -t registry.gitlab.com/lorddistrict/athena:athena-backend-test . -f docker/backend/test/Dockerfile
	docker push registry.gitlab.com/lorddistrict/athena:athena-backend-test

#########
# SWARM #
#########

.PHONY: swarm_dev
swarm_dev:
	docker stack deploy --compose-file docker-compose-dev.yml athena_dev
	docker stack deploy --compose-file docker-compose-traefik.yml athena_traefik

.PHONY: swarm_prod
swarm_prod:
	docker stack deploy --compose-file docker-compose-prod.yml athena_prod
	docker stack deploy --compose-file docker-compose-traefik.yml athena_traefik

.PHONY: swarm_test
swarm_test:
	docker stack deploy --compose-file docker-compose-test.yml athena_test
	docker stack deploy --compose-file docker-compose-traefik.yml athena_traefik