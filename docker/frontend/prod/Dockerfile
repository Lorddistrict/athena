FROM php:8.0-fpm AS backend

RUN apt-get update && \
    apt-get install -y libzip-dev zip gnupg && \
    docker-php-ext-configure zip && \
    docker-php-ext-install zip && \
    docker-php-ext-install pdo_mysql && \
    docker-php-ext-install mysqli

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=2.0.8

WORKDIR /var/www/html
COPY ./api .
RUN cp .env .env.local
RUN sed -i 's/custom_environment/prod/' .env.local
RUN composer install
RUN php bin/console assets:install public

########################################################################################################################

FROM nginx:latest

RUN apt-get update && \
    apt-get install -y vim git net-tools

WORKDIR /var/www/html
COPY ./docker/frontend/config /etc/nginx/conf.d
RUN sed -i 's/env_backend/prod_backend/' /etc/nginx/conf.d/default.conf
COPY --from=backend /var/www/html/public public